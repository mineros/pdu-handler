var apcPdu = require("apc-pdu-snmp");
const config = require("./config");
var pry = require("pryjs");

const pdus = config.pdus.map(item => {
  return new apcPdu(item);
});

function outletNames(pdus) {
  return Promise.all(
    pdus.map(pdu => {
      return new Promise((resolve, reject) => {
        pdu.getAllOutletNames(function(err, names) {
          if (err) {
            return reject(err);
          }
          return resolve({ name: pdu.options.name, outlets: names });
        });
      });
    })
  );
}

function pduPowerDraw(pdus) {
  return Promise.all(
    pdus.map(pdu => {
      return new Promise((resolve, reject) => {
        pdu.getPowerDraw(function(err, amps) {
          if (err) {
            return reject(err);
          }
          pdu_name = `pdu_${pdu.options.name}`;
          return resolve({ [pdu_name]: amps });
        });
      });
    })
  );
}

function currentLoad(pdus) {
  return Promise.all(
    pdus.map(pdu => {
      return new Promise((resolve, reject) => {
        pdu.getLoadState(function(err, state) {
          pdu_name = `pdu_${pdu.options.name}`;
          if (err) {
            return reject(err);
          }
          return resolve({ [pdu_name]: state });
        });
      });
    })
  );
}

function findOutletPdu(name, pdus) {
  return new Promise((resolve, reject) => {
    outletNames(pdus).then(pdu_objs => {
      var found_items = pdu_objs.map(pdu => {
        outlet_nums = Object.keys(pdu.outlets);
        let found_outlets = outlet_nums.filter(outlet_number => {
          return pdu.outlets[outlet_number].toLowerCase() == name.toLowerCase();
        });
        if (found_outlets.length > 0) {
          return { pdu: pdu, pdu_name: pdu.name, outlets: found_outlets };
        }
      });
      if (found_items.length > 0) {
        resolve(found_items.filter(n => n));
      } else {
        reject(`${hostname} was not found on any pdus`);
      }
    });
  });
}

// outletNames(pdus).then(names => {
//   console.log(names);
// });
// pduPowerDraw(pdus).then(amps => {
//   console.log(amps);
// });
// currentLoad(pdus).then(names => {
//   console.log(names);
// });

function reboot(pdus, outlet_name, state = 3) {
  findOutletPdu(hostname, pdus).then(items => {
    items.map(pdu_item => {
      pdu_item.outlets.map(outlet => {
        pdu_item.pdu.setPowerState(outlet, state, function(err) {
          if (err) {
            console.log(err);
            return;
          }
          console.log("Successfully turned outlet 1 on");
        });
      });
    });
  });
}

var hostname = process.argv[2];
var command = process.argv[3];

if (!hostname) {
  console.log("A pdu attached system name is required ");
  console.log(`example: ${process.argv[1]} logic8 command`);
  process.exit(1);
}
if (!/reboot|off|on/i.exec(command)) {
  console.log("A command is required, reboot|off|on");
  console.log(`example: ${process.argv[1]} logic8 command`);
  process.exit(1);
}

console.log(`Looking for hostname ${hostname}`);
switch (command) {
  case "reboot":
    reboot(pdus, hostname, 3);
    break;
  case "off":
    reboot(pdus, hostname, 2);
    break;
  case "on":
    reboot(pdus, hostname, 1);
    break;
  default:
    findOutletPdu(hostname, pdus);
    break;
}
