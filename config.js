module.exports = {
  pdus: [
    {
      name: "nwops2",
      host: "192.168.0.164",
      community: "private"
    },
    {
      name: "j3llyfish1",
      host: "192.168.0.161",
      community: "private"
    },
    {
      name: "nwops1",
      host: "192.168.0.163",
      community: "private"
    }
  ]
};
