const assert = require("assert");
const spies = require("chai-spies-next");
const chai = require("chai");
const expect = chai.expect;
const chaiAsPromised = require("chai-as-promised");
const pry = require("pryjs");

chai.use(spies);

const fs = require("fs");

describe("#clean_and_downcase", () => {
  it("removes leading and trailing whitespace in keys and values", () => {
    const data = [{ " lat ": " 37 " }];
    expect(clean_and_downcase(data)).to.deep.equal([{ lat: "37" }]);
  });

  it("replaces whitespace between words in keys with underscores", () => {
    const data = [{ "lat lng": "37 -122" }];
    expect(clean_and_downcase(data)).to.deep.equal([{ lat_lng: "37 -122" }]);
  });
});
